let pairArray = [];
function pairs(str) {
    const pairString = str.slice(0, 2);
    pairArray.push(pairString.length === 1 ? pairString+'_' : pairString);
    if(str.substring(2).length > 0 ) pairs(str.substring(2));
    return pairArray;
}

// console.log(pairs('abcdef'));